Obtendo os códigos e Repositório:

Os códigos utilizados nos experimentos estão no repositório público no GitLab: https://gitlab.com/appjr/Arduino_SO

Experimento 1:

Os códigos relacionados ao experimento #1 estão na pasta “interrupts”.
O arquivo interrupts.ino contém o código relacionado aos arduinos geradores de interrupções.
Dentro da pasta “interrupts/interruptServer” está o código a ser utilizado pelo arduino a ser testado e que receberá as interrupções.

Experimento 2:

Os códigos relacionados ao experimento #2 estão na pasta “ProjetoAnalogDigital”
Na pasta “ProjetoAnalogDigital/AnalogDigital” está o código relacionado aos arduinos geradores de dados que estarão criando o sinal analógico através do conversor digital/analógico.
Na pasta “ProjetoAnalogDigital/AnalogDigitalServidor” está o código relacionado ao arduino a ser testado e que receberá os sinais analógicos.

Nas pastas “Result” estão os dados capturados nos experimentos.
O arquivo .fzz é o arquivo do Fritzing (usado para desenhar circuitos).
Os arquivos.jpg é a imagem gerada através do Fritzing representando os circuitos.




