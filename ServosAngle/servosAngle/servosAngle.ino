#include <Servo.h>

unsigned long startTime;
unsigned long execTime1 = 2600;
unsigned long execTime2 = 1400;
unsigned long execTime3 = 700;

Servo myservo1;  
Servo myservo2;  
Servo myservo3;  

int anguloFinal1 = 0;
int anguloFinal2 = 0;
int anguloFinal3 = 0;

void setup() {

  Serial.begin(9600);  
  
  myservo1.attach(8); 
  myservo2.attach(10); 
  myservo3.attach(12); 
  
  myservo1.write(0);
  myservo2.write(0);
  myservo3.write(0);
  
  delay(1000);
}

void loop() {
  myservo1.write(0);
  myservo2.write(0);
  myservo3.write(0);
  
  delay(1000);
  int pos = 0;  
  unsigned long startTime;

  startTime = millis();
  for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    myservo1.write(pos);
    delay(15);
    if(millis()-startTime > execTime1){
      anguloFinal1 = pos;
      break;
    }
  }

  startTime = millis();
  for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    myservo2.write(pos);
    delay(15);
    if(millis()-startTime > execTime2){
      anguloFinal2 = pos;
      break;
    }
  }

  startTime = millis();
  for (pos = 0; pos <= 180; pos += 1) { // goes from 0 degrees to 180 degrees
    myservo3.write(pos);
    delay(15);
    if(millis()-startTime > execTime3){
      anguloFinal3 = pos;      
      break;
    }

  }

  printAngulos();
  
}


void printAngulos(){

  Serial.print("Angulo_1 ");
  Serial.print(anguloFinal1);

  Serial.print(" Angulo_2 ");
  Serial.print(anguloFinal2);

  Serial.print(" Angulo_3 ");
  Serial.println(anguloFinal3);
  
}

