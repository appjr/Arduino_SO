int startPin = 2;
volatile int doInterrupt = 8;

void setup() {
  pinMode(doInterrupt, OUTPUT);
  pinMode(startPin, INPUT);
  attachInterrupt(digitalPinToInterrupt(startPin), doInterruptions, FALLING);

}

void loop() {

}

void doInterruptions(){
  cli();
  for(int count = 0;count<=100;count++){
      digitalWrite(doInterrupt, HIGH);
      delay(2);
      digitalWrite(doInterrupt, LOW);
      delay(2);//variável entre diferentes arduinos
  }
  sei();
}

