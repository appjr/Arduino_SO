const byte startInterrupts = 13;

const byte interruptPin2 = 2;
const byte interruptPin3 = 3;
const byte interruptPin18 = 18;

volatile long count2 = 0;
volatile long count3 = 0;
volatile long count18 = 0;

void setup() {

  Serial.begin(9600); 
  
  pinMode(startInterrupts, OUTPUT);
  
  pinMode(interruptPin2, INPUT);
  attachInterrupt(digitalPinToInterrupt(interruptPin2), countinterruptPin2, FALLING);

  pinMode(interruptPin3, INPUT);
  attachInterrupt(digitalPinToInterrupt(interruptPin3), countinterruptPin3, FALLING);

  pinMode(interruptPin18, INPUT);
  attachInterrupt(digitalPinToInterrupt(interruptPin18), countinterruptPin18, FALLING);

  delay(3000);
}

void loop() {
  count2 = 0;
  count3 = 0;
  count18 = 0;
  Serial.print("Iniciando clientes...");
  digitalWrite(startInterrupts, HIGH);
  delayMicroseconds(50);
  digitalWrite(startInterrupts, LOW);
  delay(3000);
  printResults();

}

void countinterruptPin2() {
  //cli();
  count2 = count2 +1;
  //sei();
}

void countinterruptPin3() {
  //cli();
  count3 = count3 +1;
  //sei();
}

void countinterruptPin18() {
  //cli();
  count18 = count18 +1;
  //sei();
}

void printResults(){
  Serial.print("Resultado, ");  
  Serial.print(", Arduino 1, ");  
  Serial.print(count2); 
  Serial.print(", Arduino 2, ");  
  Serial.print(count3);  
  Serial.print(", Arduino 3, ");  
  Serial.println(count18);  
}

