#include <Wire.h>
#include <Adafruit_MCP4725.h>

Adafruit_MCP4725 dac;
float medidaAnteriorA0 = 0;
float medidaAnteriorA1 = 0;
float medidaAnteriorA2 = 0;

unsigned long perdidaA0 = 0L;
unsigned long perdidaA1 = 0L;
unsigned long perdidaA2 = 0L;

void setup(void) {
  Serial.begin(9600);
  Serial.println("Iniciando operação");
}

void loop() {
  delay(1000);
  unsigned long nTimes = 100L;
  runNTimes(nTimes);
  printPerdidas(nTimes);
  resetPerdidas();

  nTimes = 1000L;
  runNTimes(nTimes);
  printPerdidas(nTimes);
  resetPerdidas();

  nTimes = 10000L;
  runNTimes(nTimes);
  printPerdidas(nTimes);
  resetPerdidas();

  nTimes = 100000L;
  runNTimes(nTimes);
  printPerdidas(nTimes);
  resetPerdidas();

  nTimes = 1000000L;
  runNTimes(nTimes);
  printPerdidas(nTimes);
  resetPerdidas();

  nTimes = 1000000000L;
  runNTimes(nTimes);
  printPerdidas(nTimes);
  resetPerdidas();

  nTimes = 4000000000L;  
  runNTimes(nTimes);
  printPerdidas(nTimes);
  resetPerdidas();
}

void resetPerdidas(){
  perdidaA0 = 0L;
  perdidaA1 = 0L;
  perdidaA2 = 0L;
}

void runNTimes(unsigned long ntimes){
  
    for (unsigned long counter = 0; counter <= ntimes; counter++){
      
      int sensorValue = analogRead(A0);
      float voltagemMedida = sensorValue * (5.0 / 1023.0);
      perdidaA0 = perdidaA0 + keepResult(voltagemMedida, medidaAnteriorA0);
      medidaAnteriorA0 = voltagemMedida;

      sensorValue = analogRead(A1);
      voltagemMedida = sensorValue * (5.0 / 1023.0);
      perdidaA1 = perdidaA1 + keepResult(voltagemMedida, medidaAnteriorA1);
      medidaAnteriorA1 = voltagemMedida;

      sensorValue = analogRead(A2);
      voltagemMedida = sensorValue * (5.0 / 1023.0);
      perdidaA2 = perdidaA2 + keepResult(voltagemMedida, medidaAnteriorA2);
      medidaAnteriorA2 = voltagemMedida;
    }
}

int keepResult(float medida, float anterior){
    int perdida = 0;
    if(abs(medida-anterior)>0.01f){ //detecta nível possível de medida perdido
      if(medida!=0){
        perdida++;
      } 
    }
    return perdida;
}

void printPerdidas(int nVezes){
      Serial.print("Número de dados lidos:,");
      Serial.print(nVezes);
      Serial.print(",Perdidas 0,  ");
      Serial.print(perdidaA0);
      Serial.print(",Perdidas 1,  ");
      Serial.print(perdidaA1);
      Serial.print(",Perdidas 2,");
      Serial.println(perdidaA2);
}

