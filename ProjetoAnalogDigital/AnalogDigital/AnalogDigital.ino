#include <Wire.h>
#include <Adafruit_MCP4725.h>

Adafruit_MCP4725 dac;
float medidaAnterior = 0;
int perdida = 0;

void setup(void) {
  Serial.begin(9600);
  Serial.println("Hello!");
  dac.begin(0x62);
  TWBR = ((F_CPU /400000l) - 16) / 2; 
  Serial.println("Iniciando operação");
  
}

void loop(void) {
    float counter;
    float aumento = (4095/3)/680;
    for (counter = 0; counter <= (4095/3); counter+=aumento){//gera curva triângulo
      dac.setVoltage(counter, false);
      Serial.print("Voltagem relativa");
      Serial.println(counter);
    }
}
